from flask import Flask
from pytest import fixture
import csv

from app import create_app


@fixture
def app():
    return create_app()


@fixture
def client(app: Flask):
    return app.test_client()


def test_signup_sucess(client):
    response = client.post('/signup',
    json={
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "password": "imgoingtobeahokage123",
        "age": 19})

    expected = {
        "id": "1",
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "age": 19
    }

    assert response.get_json() == expected
    assert response.status == '201 CREATED'


def test_singup_failure(client):
    response = client.post('/signup',
    json={
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "password": "imgoingtobeahokage123",
        "age": 19})

    assert response.status == '422 UNPROCESSABLE ENTITY'
    assert response.get_json() == {}


def test_login_sucess(client):
    response = client.post('/login',
    json={
        "email": "naruto@konoha.com",
        "password": "imgoingtobeahokage123",
    })

    expected = {
        "id": "1",
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "age": 19
    }

    assert response.get_json() == expected
    assert response.status == '200 OK'


def test_login_failure(client):
    response = client.post('/login',
    json={
        "email": "naruto@konoha.com",
        "password": "imgoingtobeahokage1234",
    })

    expected = {
        "message": "incorrect email or password"
    }

    assert response.get_json() == expected
    assert response.status == '401 UNAUTHORIZED'


def test_profile_patch_sucess(client):
    response = client.patch('/profile/1',
    json={
        "age": 21
    })

    expected = {
        "id": "1",
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "age": 21
    }

    assert response.get_json() == expected
    assert response.status == '200 OK'


def test_profile_patch_failure(client):
    response = client.patch('/profile/8',
    json={
        "age": 21
    })

    expected = {
        "message": "invalid id"
    }

    assert response.get_json() == expected
    assert response.status == '404 NOT FOUND'


def test_users(client):
    response = client.get('/users')

    expected = {
        "data": [
            {          
                "id": "1",
                "name": "Naruto Uzumaki",
                "email": "naruto@konoha.com",
                "age": 21
            }
        ]
    }

    assert response.get_json() == expected
    assert response.status == '200 OK'


def test_profile_delete_sucess(client):
    response = client.delete('/profile/1')

    assert response.status == '204 NO CONTENT'


def test_profile_delete_failure(client):
    response = client.delete('/profile/1')

    expected = {
        "message": "invalid id"
    }

    assert response.get_json() == expected
    assert response.status == '404 NOT FOUND'