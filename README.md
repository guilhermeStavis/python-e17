- Foi desenvolvido primeiro a rota de registro, onde foi verificado se o nome para cadastrar já existe, para decidir o status e resposta adequado, 201 para sucesso, 422 para fracasso.

- A próxima rota desenvolvida foi a rota de login, na qual há a verificação da combinação email/password de entrada, devolvendo um satus 200 se estiver tudo certo, ou um 401 caso tenha algum erro.

- Após a rota de login, foi dado atenção à rota de usuários, trazendo a listagem dos usuários cadastrados.

- Por fim foi implementada a rota de perfil, com dois métodos desenvolvidos separadamente. O método patch faz a atualização do cadastro segundo o corpo da requisição para o ID informado na URI '/profile/<int:user_id>'. Caso esse ID seja inexistente, retorna um status 404. O método delete usa a memsa URI para achar a entrada desejada, caso ela exista, retorna um 204, caso contrário um 404 também.

- Os testes automatizados cobrem todas as rotas com todas as possibilidades de sucesso ou fracasso.

- A arquitetura escolhida foi a MVTS para a produção desta API.
