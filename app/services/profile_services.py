from http import HTTPStatus
import csv

from .general_services import GeneralServices

class ProfileServices:

    @staticmethod
    def rewrite_csv(all_users):
        for i, user in enumerate(all_users):
            user['id'] = str(i + 1)

        with open('users.csv', 'w') as file:
                fieldnames = ['id', 'name', 'email', 'password', 'age']
                writer = csv.DictWriter(file, fieldnames=fieldnames, extrasaction='ignore')
                               
                writer.writeheader()
                writer.writerows(all_users)

    @staticmethod
    def update_user(user_id: int, updated_status: dict):
        all_users = GeneralServices.read_csv()

        user_to_update = [user for user in all_users if user.get('id') == str(user_id)]

        if user_to_update:
            for key in user_to_update[0].keys():
                for updated_key in updated_status.keys():
                    if key == updated_key:
                        user_to_update[0][key] = updated_status[updated_key]


            ProfileServices.rewrite_csv(all_users)

            user_output = GeneralServices.remove_sensive_data(user_to_update[0])

            return (user_output, HTTPStatus.OK,)

        return ({'message': 'invalid id'}, HTTPStatus.NOT_FOUND)

    @staticmethod
    def delete_user(user_id):
        all_users = GeneralServices.read_csv()
        user_to_delete = [user for user in all_users if user.get('id') == str(user_id)]
        
        if user_to_delete: 
            del all_users[user_id - 1]

            ProfileServices.rewrite_csv(all_users)

            return ({}, 204)

        return ({'message': 'invalid id'}, HTTPStatus.NOT_FOUND)
