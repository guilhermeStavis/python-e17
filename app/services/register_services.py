from http import HTTPStatus
import csv

from .general_services import GeneralServices

class RegisterServices:

    @staticmethod
    def register_entry(user_to_register: dict):
        if not GeneralServices.check_if_exists(user_to_register.get('email')):
            users = GeneralServices.read_csv()

            user_to_register['id'] = GeneralServices.id_gen(users)

            output = GeneralServices.remove_sensive_data(user_to_register)

            with open('users.csv', 'w') as file:
                fieldnames = ['id', 'name', 'email', 'password', 'age']
                writer = csv.DictWriter(file, fieldnames=fieldnames, extrasaction='ignore')
                
                users.append(user_to_register)
                
                writer.writeheader()
                writer.writerows(users)

            return (output, HTTPStatus.CREATED)
        return ({}, HTTPStatus.UNPROCESSABLE_ENTITY,)