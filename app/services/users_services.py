from http import HTTPStatus
import csv

from .general_services import GeneralServices

class UserServices:

    @staticmethod
    def list_all_users():
        users = GeneralServices.read_csv()
        
        users_output = []
        for user in users:
            user_output = GeneralServices.remove_sensive_data(user)

            users_output.append(user_output)

        return ({'data': users_output}, HTTPStatus.OK,)