from http import HTTPStatus
import csv

from .general_services import GeneralServices

class LoginService:

    @staticmethod
    def login(user_to_login: dict):
        users = GeneralServices.read_csv()

        correct_user = [
            user for user in users
            if user_to_login['email'] == user['email']
            and user_to_login.get('password') == user.get('password')
        ]

        if correct_user:         
            output = GeneralServices.remove_sensive_data(correct_user[0])
            
            return (output, HTTPStatus.OK)

        return ({'message': 'incorrect email or password'}, HTTPStatus.UNAUTHORIZED)

           
