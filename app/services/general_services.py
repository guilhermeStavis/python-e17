import csv

class GeneralServices:

    @staticmethod
    def id_gen(users):
        if users:
            return str(len(users) + 1)

        return '1'

    @staticmethod
    def read_csv():
        with open('users.csv') as file:
            return [user for user in csv.DictReader(file)]

    @staticmethod
    def check_if_exists(email):
        users = GeneralServices.read_csv()

        check = [user for user in users if user.get('email') == email]

        if check:
            return True

        return False

    @staticmethod
    def remove_sensive_data(user: dict):
        output = {
            'id': user.get('id'),
            'name': user.get('name'),
            'email': user.get('email'),
            'age': int(user.get('age'))
        }

        return output