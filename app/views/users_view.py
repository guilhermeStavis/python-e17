from flask import Blueprint
from ..services.users_services import UserServices

bp = Blueprint('users_route', __name__)

@bp.route('/users', methods=['GET'])
def all_users():
    res = UserServices.list_all_users()
    return res[0], res[1]