from flask import Blueprint, request

from ..services.profile_services import ProfileServices

bp_patch = Blueprint('profile_patch_route', __name__)

@bp_patch.route('/profile/<int:user_id>', methods=['PATCH'])
def update_user(user_id):
    res = ProfileServices.update_user(user_id, request.get_json())
    return res[0], res[1]

bp_delete = Blueprint('profile_delete_route', __name__)

@bp_delete.route('/profile/<int:user_id>', methods=['DELETE'])
def delete_user(user_id):
    res = ProfileServices.delete_user(user_id)
    return res[0], res[1]