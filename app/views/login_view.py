from flask import Blueprint, request
from ..services.login_service import LoginService

bp = Blueprint('login_route', __name__)

@bp.route('/login', methods=['POST'])
def login():
    res = LoginService.login(request.get_json())
    return res[0], res[1]