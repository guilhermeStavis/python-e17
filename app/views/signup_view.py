from flask import Blueprint, request
from ..services.register_services import RegisterServices


bp = Blueprint('signup_route', __name__)

@bp.route('/signup', methods=['POST'])
def signup():
    res = RegisterServices.register_entry(request.get_json())
    return res[0], res[1]
