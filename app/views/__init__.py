from .signup_view import bp as bp_signup
from .login_view import bp as bp_login
from .profile_view import bp_patch, bp_delete
from .users_view import bp as bp_users

def init_app(app):
    app.register_blueprint(bp_signup)
    app.register_blueprint(bp_login)
    app.register_blueprint(bp_patch)
    app.register_blueprint(bp_delete)
    app.register_blueprint(bp_users)